<?php
abstract class AbstractAmo
{
    abstract protected function getLeads();

    public $domain;

	public function __construct($domain, $login, $hash){
		$user=array(
  		'USER_LOGIN'=>$login,
			'USER_HASH'=>$hash
		);
		$this->domain=$domain;
		$link='https://'.$domain.'.amocrm.ru/private/api/auth.php?type=json';
		$curl=curl_init();
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_URL,$link);
		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
		curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($user));
		curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		curl_setopt($curl,CURLOPT_HEADER,false);
		curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
		curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
		$out=curl_exec($curl);
	}
}

class Amo extends AbstractAmo
{
  function __construct($domain, $login, $hash) {
    parent::__construct($domain, $login, $hash);
  }

  function getLeads() {
    $link='https://'.$this->domain.'.amocrm.ru/api/v2/leads';
    $curl=curl_init();
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    $out=curl_exec($curl);
    return $out;
  }
}

$amo = new Amo('ssvtest','ssv_test@winwin-tech.ru', 'ef4fe857c32021410df31dcc890001833eac4820');
?>
<script type="text/javascript">
const leadsString = '<?php echo $amo->getLeads();?>';
const leads = JSON.parse(leadsString);
const usersLink = 'https://winwinapp.ru/test.php';
const xhr = new XMLHttpRequest();
xhr.open('GET', usersLink, false);
xhr.send();
const users = JSON.parse(xhr.responseText);
let result = {};
for (user in users) {
  result[users[user].name] = [];
}
result = leads._embedded.items.reduce((acc,value) => {
  const responsibleUserId = value.responsible_user_id;
  if (users.hasOwnProperty(responsibleUserId)) {
    acc[users[responsibleUserId].name].push(value);
    return acc;
  }
  return acc;
}, result);
for (name in result) {
  if(result[name].length === 0) {
    delete result[name];
    }
  }
document.write(JSON.stringify(result));
</script>
